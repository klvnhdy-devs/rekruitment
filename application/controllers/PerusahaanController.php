<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PerusahaanController extends CI_Controller {

	public function __construct()
  	{
		parent::__construct();
		if($this->session->userdata('login') == FALSE)
		{
			redirect('Login');
		}
	
	}

	public function index()
	{
		$this->load->view('perusahaan/index');
	}
}
