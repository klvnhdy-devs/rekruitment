<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
 	{
    	parent::__construct();
	}
  

	public function index()
	{
		if($this->session->userdata('login')){
			redirect('Home');
		}
		$this->load->view('userpanel/login');
	}

	public function ProsesLogin()
	{
		$username = $this->input->post('email');
		$password = md5($this->input->post('password').'*');
		$cekLog = $this->mod_login->getLogin($username,$password);
		if ($cekLog->num_rows() > 0) {
			$this->session->set_userdata('login','sukses');			
		}else{
			$this->session->set_flashdata('msg', 'Username atau Password salah');
			redirect('Login');
		}
	}

	public function ProsesDaftar()
	{
		$username = $this->input->post('email');
		$password = md5($this->input->post('password').'*');
		
	}

	public function prosesLogout(){
		session_destroy();
		redirect('Login');
	}

}
