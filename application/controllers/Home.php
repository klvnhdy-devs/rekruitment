<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
  	{
		parent::__construct();
		if($this->session->userdata('login') == FALSE)
		{
			redirect('Login');
		}
	
	}

	public function index()
	{
		$this->load->view('userpanel/index');
	}

	public function assigment(){
		$this->load->view('userpanel/assigment');
	}

	public function detailJob(){
		$this->load->view('userpanel/detailJob');
	}

	public function notFound()
	{
		$this->load->view('404/index');
	}

	public function saveImage()
	{
		$folder = 'asset/images/assignment/';
		$img  = $this->input->post('image');
		$img  = str_replace('data:image/png;base64,', '', $img);
		$img  = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = $folder . mktime(date('H'), date('i'), date('s'), date('n'), date('j'), date('Y')) . ".png";
		$upload = file_put_contents($file, $data);
		echo $upload ? $file : "gagal";
	}

}
