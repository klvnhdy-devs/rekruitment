<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title></title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<link href="<?=base_url('/asset/metronic')?>/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('/asset/custome')?>/style.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('/asset/metronic')?>/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('/asset/metronic')?>/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('/asset/metronic')?>/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		


		
		<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.ico" />
	</head>
	<body class="m--skin- m-page--loading-enabled m-page--loading m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default">
        