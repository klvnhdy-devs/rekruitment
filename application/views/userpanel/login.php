<?php $this->load->view('template/header');?>
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
            <span>Please wait...</span>
            <span>
                <div class="m-loader m-loader--brand"></div>
            </span>
        </div>
    </div>

    <?php $this->load->view('template/navigasi');?> 

    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
               <!-- BEGIN: Subheader -->
               <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title "></h3>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                    <div class="row">
                        <div class="col-xl-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="m-portlet">
                                        <div class="m-portlet__head">
                                            <div class="m-portlet__head-caption">
                                                <div class="m-portlet__head-title">
                                                    <span class="m-portlet__head-icon">
                                                        <i class="la la-thumb-tack m--font-success"></i>
                                                    </span>
                                                    <h3 class="m-portlet__head-text m--font-success">
                                                        PT. Perinapan Official
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="m-portlet__head-tools">
                                                <ul class="m-portlet__nav">
                                                    <li class="m-portlet__nav-item">
                                                        <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
                                                            <i class="flaticon-share"></i>
                                                        </a>
                                                    </li>
                                                    <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
                                                        <a href="#" class="m-portlet__nav-link btn btn-secondary  m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill   m-dropdown__toggle">
                                                            <i class="fa fa-share"></i>
                                                        </a>
                                                        <div class="m-dropdown__wrapper">
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            <li class="m-nav__item">
                                                                                <a href="#" class="btn btn-outline-success m-btn m-btn--pill m-btn--wide btn-sm">Simpan</a>
                                                                                <a href="#" class="btn btn-outline-success m-btn m-btn--pill m-btn--wide btn-sm">View Job</a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="m-portlet__body">
                                            <h4>BackEnd Developer ( 4 - 6 Juta)</h4>
                                        Kandidat harus memiliki setidaknya Gelar Pasca Sarjana di bidang apapun. Setidaknya memiliki 3 tahun pengalaman dalam bidang yang sesuai untuk posisi Kandidat harus memiliki setidaknya Gelar Pasca Sarjana di bidang apapun. Setidaknya memiliki 3 tahun pengalaman dalam bidang yang sesuai untuk posisi... 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div id="Daftar" class="card-shadow card-daftar m-portlet m-portlet--fit">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <div class="row">
                                                <h3 class="m-portlet__head-text">
                                                    <button class="btn btn-outline-success"> Daftar  </button>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="container" id="type">
                                    <br><br><br>
                                        <div class="row">
                                            <div class="col-5 type-login" onclick="updateType('Perusahaan')">
                                                <center>
                                                    <br>
                                                    <br>
                                                    <i class="flaticon-suitcase" style="font-size:40px;"></i>
                                                    <h3>
                                                        Perusahaan
                                                    </h4>
                                                </center>
                                            </div>
                                            <div class="col-2"></div>
                                            <div class="col-5 type-login"  onclick="updateType('Pelamar')">
                                                <center>
                                                    <br>
                                                    <br>
                                                    <i class="flaticon-suitcase" style="font-size:40px;"></i>
                                                    <h3>
                                                        Pelamar
                                                    </h4>
                                                </center>
                                            </div>
                                        </div>
                                         <br><br><br>
                                        <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                            <center>
                                                <a href="#" onclick="sliderCardDaftar()" id="daftar"> Login </a>
                                            </center>
                                        </div>
                                    </div>
                                    <form action="" id="formDaftar">
                                        <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                            <input type="text" autocomplete="off" class="form-control m-input" name="username" placeholder="Username" data-toggle="m-tooltip" title="">
                                        </div>
                                        <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                            <input type="text" autocomplete="off" class="form-control m-input" name="email" placeholder="Email" data-toggle="m-tooltip" title="">
                                        </div>
                                        <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                            <input type="text" class="form-control m-input" name="password" placeholder="Password" data-toggle="m-tooltip" title="">
                                            <input type="hidden" id="typeLogin" class="form-control m-input" name="type">
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                            <button onclick="cekDaftar()" class="btn btn-success col-md-12 col-sm-12 mb-3">  Daftar </button>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                            <center>
                                                <a href="#" onclick="sliderCardDaftar()" id="daftar"> Login </a>
                                            </center>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="login" class="card-shadow card-login m-portlet m-portlet--fit">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <div class="row">
                                                <h3 class="m-portlet__head-text">
                                                    <button class="btn btn-outline-success"> Login  </button>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <form action="<?=base_url('Proses')?>" method="POST">
                                        <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                            <label for=""> Email </label>
                                        </div>
                                        <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                            <input type="email" autocomplete="off" class="form-control m-input" name="email" placeholder="Email" data-toggle="m-tooltip" title="" require>
                                        </div>
                                        <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                            <label for=""> Password </label>
                                        </div>
                                        <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                            <input type="password" class="form-control m-input" name="password" placeholder="Password" data-toggle="m-tooltip" title="" required>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                            <button type="submit" class="btn btn-success col-md-12 col-sm-12 mb-3">  Login </button>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                            <center>
                                                <a href="#" onclick="sliderCard()" id="daftar"> Daftar </a>
                                            </center>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $this->load->view('template/footer');?>
