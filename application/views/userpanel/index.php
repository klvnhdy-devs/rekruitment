<?php $this->load->view('template/header');?>
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
            <span>Please wait...</span>
            <span>
                <div class="m-loader m-loader--brand"></div>
            </span>
        </div>
    </div>
    
    <?php $this->load->view('template/navigasi');?>    
    
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">Lowongan di Indonesia</h3>
                    </div>
                </div>
            </div>

            <!-- END: Subheader -->
            <div class="m-content">

                <!--Begin::Section-->
                <div class="row">
                    <div class="col-xl-4">

                        <!--begin:: Widgets/Finance Summary-->
                        <div id="cardKriteria" class="m-portlet m-portlet--fit">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Pilih Kriteria
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                    <input type="text" class="form-control m-input" name="keyword" placeholder="Judul posisi atau kata kunci" data-toggle="m-tooltip" title="">
                                </div>
                                <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                    <input type="text" class="form-control m-input" name="keyword" placeholder="Pilih lokasi" data-toggle="m-tooltip" title="">
                                </div>
                                <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                    <input type="text" class="form-control m-input" name="keyword" placeholder="Spesialis" data-toggle="m-tooltip" title="">
                                </div>
                                <div class="col-lg-12 col-md-9 col-sm-12 mb-3">
                                    <input type="text" class="form-control m-input" name="keyword" placeholder="Range gaji" data-toggle="m-tooltip" title="">
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 mb-3">
                                    <button class="btn btn-success col-md-12 col-sm-12 mb-3">  Cari </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <div class="row">
                            <?php 
                                for ($i=0; $i < 5 ; $i++) {  ?>
                            <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-thumb-tack m--font-success"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-success">
													PT. Perinapan Official
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill">
														<i class="flaticon-share"></i>
													</a>
												</li>
												<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
													<a href="#" class="m-portlet__nav-link btn btn-secondary  m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill   m-dropdown__toggle">
														<i class="fa fa-share"></i>
													</a>
													<div class="m-dropdown__wrapper">
														<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
														<div class="m-dropdown__inner">
															<div class="m-dropdown__body">
																<div class="m-dropdown__content">
																	<ul class="m-nav">
																		<li class="m-nav__item">
																			<a href="#" class="btn btn-outline-success m-btn m-btn--pill m-btn--wide btn-sm">Simpan</a>
																			<a href="#" class="btn btn-outline-success m-btn m-btn--pill m-btn--wide btn-sm">View Job</a>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
                                        <h4>BackEnd Developer ( 4 - 6 Juta)</h4>
                                    Kandidat harus memiliki setidaknya Gelar Pasca Sarjana di bidang apapun. Setidaknya memiliki 3 tahun pengalaman dalam bidang yang sesuai untuk posisi Kandidat harus memiliki setidaknya Gelar Pasca Sarjana di bidang apapun. Setidaknya memiliki 3 tahun pengalaman dalam bidang yang sesuai untuk posisi... 
									</div>
								</div>
                            </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Body -->

<!-- begin::Footer -->
<footer class="m-grid__item  m-footer ">
    <div class="m-container m-container--responsive m-container--xxl m-container--full-height">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                <span class="m-footer__copyright">
                    
                </span>
            </div>
            <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">About</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Privacy</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">T&C</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Purchase</span>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item--last">
                        <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                            <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- end::Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Quick Sidebar -->
<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
<div class="m-quick-sidebar__content m--hide">
    <span id="m_quick_sidebar_close" class="m-quick-sidebar__close"><i class="la la-close"></i></span>
    <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
        <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">Messages</a>
        </li>
        <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">Settings</a>
        </li>
        <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">Logs</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
            <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
                <div class="m-messenger__messages m-scrollable">
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--in">
                            <div class="m-messenger__message-pic">
                                <img src="assets/app/media/img//users/user3.jpg" alt="" />
                            </div>
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                        Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                        Hi Bob. What time will be the meeting ?
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--out">
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                        Hi Megan. It's at 2.30PM
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--in">
                            <div class="m-messenger__message-pic">
                                <img src="assets/app/media/img//users/user3.jpg" alt="" />
                            </div>
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                        Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                        Will the development team be joining ?
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--out">
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                        Yes sure. I invited them as well
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__datetime">2:30PM</div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--in">
                            <div class="m-messenger__message-pic">
                                <img src="assets/app/media/img//users/user3.jpg" alt="" />
                            </div>
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                        Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                        Noted. For the Coca-Cola Mobile App project as well ?
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--out">
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                        Yes, sure.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--out">
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                        Please also prepare the quotation for the Loop CRM project as well.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__datetime">3:15PM</div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--in">
                            <div class="m-messenger__message-no-pic m--bg-fill-danger">
                                <span>M</span>
                            </div>
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                        Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                        Noted. I will prepare it.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--out">
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                        Thanks Megan. I will see you later.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--in">
                            <div class="m-messenger__message-pic">
                                <img src="assets/app/media/img//users/user3.jpg" alt="" />
                            </div>
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-username">
                                        Megan wrote
                                    </div>
                                    <div class="m-messenger__message-text">
                                        Sure. See you in the meeting soon.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-messenger__seperator"></div>
                <div class="m-messenger__form">
                    <div class="m-messenger__form-controls">
                        <input type="text" name="" placeholder="Type here..." class="m-messenger__form-input">
                    </div>
                    <div class="m-messenger__form-tools">
                        <a href="" class="m-messenger__form-attachment">
                            <i class="la la-paperclip"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="m_quick_sidebar_tabs_settings" role="tabpanel">
            <div class="m-list-settings m-scrollable">
                <div class="m-list-settings__group">
                    <div class="m-list-settings__heading">General Settings</div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Email Notifications</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" checked="checked" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Site Tracking</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">SMS Alerts</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Backup Storage</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Audit Logs</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" checked="checked" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-list-settings__group">
                    <div class="m-list-settings__heading">System Settings</div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">System Logs</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Error Reporting</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Applications Logs</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Backup Servers</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" checked="checked" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                    <div class="m-list-settings__item">
                        <span class="m-list-settings__item-label">Audit Logs</span>
                        <span class="m-list-settings__item-control">
                            <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
                                <label>
                                    <input type="checkbox" name="">
                                    <span></span>
                                </label>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- end::Quick Sidebar -->


<?php $this->load->view('template/footer');?>
	