<?php $this->load->view('template/header');?>
<?php $this->load->view('template/navigasi');?>
  
    <video id="video" width="640" height="480" autoplay></video>
    <canvas id="canvas" width="640" height="480" style="width: 640px;"></canvas>
<?php $this->load->view('template/footer');?>
<script>
    var video = document.getElementById('video');
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.srcObject = stream;
            video.play();
        });
    }

    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var video = document.getElementById('video');
    var myVar = setInterval(takePickture, 10000);

    function takePickture(){
        context.drawImage(video, 0, 0, 640, 480);
        var image = new Image();
        image = canvas.toDataURL("image/png");
        $.ajax({
                type: "POST",
                url: "<?=base_url('savePict')?>",
                data: { 
                    image: image
                }
            }).done(function(res) {
                console.log(res); 
            });
    }
</script>