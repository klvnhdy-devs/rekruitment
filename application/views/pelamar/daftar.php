<?php $this->load->view('template/header');?>
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
            <span>Please wait...</span>
            <span>
                <div class="m-loader m-loader--brand"></div>
            </span>
        </div>
    </div>
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="row">
                        <div class="col-xl-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="m-portlet card-shadow">
                                        <div class="m-portlet__head">
                                            <div class="m-portlet__head-caption">
                                                <div class="m-portlet__head-title">
                                                    <span class="m-portlet__head-icon">
                                                        <i class="la la-thumb-tack m--font-success"></i>
                                                    </span>
                                                    <h3 id="pageInfo" class="m-portlet__head-text m--font-success" >
                                                        Detail Personal 
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <form id="personalData" class="m-form m-form--fit m-form--label-align-right">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Full Name:</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Email</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Nomor Telepon</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Alamat</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                            </div>     
                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                <div class="m-form__actions m-form__actions--solid">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <button type="reset" class="btn btn-primary">Back</button>
                                                        </div>
                                                        <div class="col-lg-6 m--align-right">
                                                            <button type="reset" onclick="personalData()" class="btn btn-danger">Next</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       
                                        </form>
                                        <form id="educationData" class="m-form m-form--fit m-form--label-align-right">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Name Of Last Education</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Jurusan</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Nomor Telepon</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Alamat</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                            </div>     
                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                <div class="m-form__actions m-form__actions--solid">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <button type="reset" class="btn btn-primary">Back</button>
                                                        </div>
                                                        <div class="col-lg-6 m--align-right">
                                                            <button type="reset" onclick="educationData()" class="btn btn-danger">Next</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       
                                        </form>
                                        <form id="experienceData" class="m-form m-form--fit m-form--label-align-right">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Experience</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Jurusan</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Nomor Telepon</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Alamat</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                            </div>     
                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                <div class="m-form__actions m-form__actions--solid">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <button type="reset" class="btn btn-primary">Back</button>
                                                        </div>
                                                        <div class="col-lg-6 m--align-right">
                                                            <button type="reset" onclick="experienceData()" class="btn btn-danger">Next</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       
                                        </form>
                                        <form id="skillData" class="m-form m-form--fit m-form--label-align-right">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Skill</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Jurusan</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Nomor Telepon</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Alamat</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                            </div>     
                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                <div class="m-form__actions m-form__actions--solid">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <button type="reset" class="btn btn-primary">Back</button>
                                                        </div>
                                                        <div class="col-lg-6 m--align-right">
                                                            <button type="reset" onclick="skillData()" class="btn btn-danger">Next</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       
                                        </form>
                                        <form id="summaryData" class="m-form m-form--fit m-form--label-align-right">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Summary</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Jurusan</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Nomor Telepon</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-form-label">Alamat</label>
                                                    <input type="email" class="form-control m-input" placeholder="Enter full name">
                                                    <span class="m-form__help">Please enter your full name</span>
                                                </div>
                                            </div>     
                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                <div class="m-form__actions m-form__actions--solid">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <button type="reset" class="btn btn-primary">Back</button>
                                                        </div>
                                                        <div class="col-lg-6 m--align-right">
                                                            <button type="reset" class="btn btn-danger">Next</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="card-shadow card-login m-portlet m-portlet--fit" style="height:340px!important">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <div class="row">
                                                <h3 class="m-portlet__head-text">
                                                    Foto Profile
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <center>
                                        <img  width="245px!important" src="<?=base_url('asset/metronic/assets2/app/media/img/blog/blog1.jpg')?>" alt="">
                                    </center>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions--solid">
                                    <div class="container">
                                        <div class="row">
                                            <button  class="col-lg-12 btn btn-primary">Unggah File</button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $this->load->view('template/footer');?>
